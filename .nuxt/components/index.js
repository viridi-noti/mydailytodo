export { default as AddTaskDialog } from '../..\\components\\AddTaskDialog.vue'
export { default as CommonContainer } from '../..\\components\\CommonContainer.vue'
export { default as ConfirmationDialog } from '../..\\components\\ConfirmationDialog.vue'
export { default as DailyTaskList } from '../..\\components\\DailyTaskList.vue'
export { default as DefaultTaskList } from '../..\\components\\DefaultTaskList.vue'
export { default as FileImportDialog } from '../..\\components\\FileImportDialog.vue'
export { default as FilterGroup } from '../..\\components\\FilterGroup.vue'
export { default as FloatingButtonGroup } from '../..\\components\\FloatingButtonGroup.vue'
export { default as Notification } from '../..\\components\\Notification.vue'
export { default as StringPromptDialog } from '../..\\components\\StringPromptDialog.vue'
export { default as Task } from '../..\\components\\Task.vue'
export { default as TimePicker } from '../..\\components\\TimePicker.vue'
export { default as FloatingButtonGroupItem } from '../..\\components\\FloatingButtonGroup\\Item.vue'

export const LazyAddTaskDialog = import('../..\\components\\AddTaskDialog.vue' /* webpackChunkName: "components/add-task-dialog" */).then(c => c.default || c)
export const LazyCommonContainer = import('../..\\components\\CommonContainer.vue' /* webpackChunkName: "components/common-container" */).then(c => c.default || c)
export const LazyConfirmationDialog = import('../..\\components\\ConfirmationDialog.vue' /* webpackChunkName: "components/confirmation-dialog" */).then(c => c.default || c)
export const LazyDailyTaskList = import('../..\\components\\DailyTaskList.vue' /* webpackChunkName: "components/daily-task-list" */).then(c => c.default || c)
export const LazyDefaultTaskList = import('../..\\components\\DefaultTaskList.vue' /* webpackChunkName: "components/default-task-list" */).then(c => c.default || c)
export const LazyFileImportDialog = import('../..\\components\\FileImportDialog.vue' /* webpackChunkName: "components/file-import-dialog" */).then(c => c.default || c)
export const LazyFilterGroup = import('../..\\components\\FilterGroup.vue' /* webpackChunkName: "components/filter-group" */).then(c => c.default || c)
export const LazyFloatingButtonGroup = import('../..\\components\\FloatingButtonGroup.vue' /* webpackChunkName: "components/floating-button-group" */).then(c => c.default || c)
export const LazyNotification = import('../..\\components\\Notification.vue' /* webpackChunkName: "components/notification" */).then(c => c.default || c)
export const LazyStringPromptDialog = import('../..\\components\\StringPromptDialog.vue' /* webpackChunkName: "components/string-prompt-dialog" */).then(c => c.default || c)
export const LazyTask = import('../..\\components\\Task.vue' /* webpackChunkName: "components/task" */).then(c => c.default || c)
export const LazyTimePicker = import('../..\\components\\TimePicker.vue' /* webpackChunkName: "components/time-picker" */).then(c => c.default || c)
export const LazyFloatingButtonGroupItem = import('../..\\components\\FloatingButtonGroup\\Item.vue' /* webpackChunkName: "components/floating-button-group-item" */).then(c => c.default || c)
