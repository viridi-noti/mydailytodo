import Vue from 'vue'

const components = {
  AddTaskDialog: () => import('../..\\components\\AddTaskDialog.vue' /* webpackChunkName: "components/add-task-dialog" */).then(c => c.default || c),
  CommonContainer: () => import('../..\\components\\CommonContainer.vue' /* webpackChunkName: "components/common-container" */).then(c => c.default || c),
  ConfirmationDialog: () => import('../..\\components\\ConfirmationDialog.vue' /* webpackChunkName: "components/confirmation-dialog" */).then(c => c.default || c),
  DailyTaskList: () => import('../..\\components\\DailyTaskList.vue' /* webpackChunkName: "components/daily-task-list" */).then(c => c.default || c),
  DefaultTaskList: () => import('../..\\components\\DefaultTaskList.vue' /* webpackChunkName: "components/default-task-list" */).then(c => c.default || c),
  FileImportDialog: () => import('../..\\components\\FileImportDialog.vue' /* webpackChunkName: "components/file-import-dialog" */).then(c => c.default || c),
  FilterGroup: () => import('../..\\components\\FilterGroup.vue' /* webpackChunkName: "components/filter-group" */).then(c => c.default || c),
  FloatingButtonGroup: () => import('../..\\components\\FloatingButtonGroup.vue' /* webpackChunkName: "components/floating-button-group" */).then(c => c.default || c),
  Notification: () => import('../..\\components\\Notification.vue' /* webpackChunkName: "components/notification" */).then(c => c.default || c),
  StringPromptDialog: () => import('../..\\components\\StringPromptDialog.vue' /* webpackChunkName: "components/string-prompt-dialog" */).then(c => c.default || c),
  Task: () => import('../..\\components\\Task.vue' /* webpackChunkName: "components/task" */).then(c => c.default || c),
  TimePicker: () => import('../..\\components\\TimePicker.vue' /* webpackChunkName: "components/time-picker" */).then(c => c.default || c),
  FloatingButtonGroupItem: () => import('../..\\components\\FloatingButtonGroup\\Item.vue' /* webpackChunkName: "components/floating-button-group-item" */).then(c => c.default || c)
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
