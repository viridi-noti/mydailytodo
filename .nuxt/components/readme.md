# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<AddTaskDialog>` | `<add-task-dialog>` (components/AddTaskDialog.vue)
- `<CommonContainer>` | `<common-container>` (components/CommonContainer.vue)
- `<ConfirmationDialog>` | `<confirmation-dialog>` (components/ConfirmationDialog.vue)
- `<DailyTaskList>` | `<daily-task-list>` (components/DailyTaskList.vue)
- `<DefaultTaskList>` | `<default-task-list>` (components/DefaultTaskList.vue)
- `<FileImportDialog>` | `<file-import-dialog>` (components/FileImportDialog.vue)
- `<FilterGroup>` | `<filter-group>` (components/FilterGroup.vue)
- `<FloatingButtonGroup>` | `<floating-button-group>` (components/FloatingButtonGroup.vue)
- `<Notification>` | `<notification>` (components/Notification.vue)
- `<StringPromptDialog>` | `<string-prompt-dialog>` (components/StringPromptDialog.vue)
- `<Task>` | `<task>` (components/Task.vue)
- `<TimePicker>` | `<time-picker>` (components/TimePicker.vue)
- `<FloatingButtonGroupItem>` | `<floating-button-group-item>` (components/FloatingButtonGroup/Item.vue)
