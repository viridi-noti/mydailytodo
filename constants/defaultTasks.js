export const DEFAULT_TASKS = [
  {
    id: 1,
    name: 'Rửa bát buổi sáng'
  },
  {
    id: 2,
    name: 'Đun nước'
  },
  {
    id: 3,
    name: 'Chuẩn bị bữa sáng'
  },
  {
    id: 4,
    name: 'Chuẩn bị đồ vợ mang đi'
  },
  {
    id: 5,
    name: 'Ăn sáng'
  },
  {
    id: 6,
    name: 'Đi vệ sinh'
  },
  {
    id: 7,
    name: 'Sắp xếp đồ mang đi của mình'
  },
  {
    id: 8,
    name: 'Ghi lại chi tiêu gần đây'
  },
  {
    id: 9,
    name: 'Xếp bát ăn cơm tối'
  },
  {
    id: 10,
    name: 'Ăn tối'
  },
  {
    id: 11,
    name: 'Rửa bát buổi tối'
  },
  {
    id: 12,
    name: 'Đi tắm'
  },
  {
    id: 13,
    name: 'Giặt quần áo'
  },
  {
    id: 14,
    name: 'Phơi quần áo'
  }
];
