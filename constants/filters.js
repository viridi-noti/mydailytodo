export default {
  ALL: 'All',
  DONE: 'Done',
  NOT_DONE: 'Not done',
  BY_DEADLINE: 'By deadline',
  PINNED_ONLY: 'Pinned tasks'
};
