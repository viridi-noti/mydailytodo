export default {
  components: true,
  modules: ['@nuxt/components', '@nuxtjs/pwa', '@nuxtjs/vuetify'],
  pwa: {
    meta: {
      theme_color: '#8A307F'
    }
  },
  css: ['@/assets/scss/main.scss'],
  plugins: [
    { src: '~/plugins/resize', ssr: false, mode: 'client' },
    { src: '~/plugins/localStorage', ssr: false, mode: 'client' },
    { src: '~/plugins/vuedraggable' }
  ],
  build: {
    extractCSS: {
      ignoreOrder: false
    }
  }
};
