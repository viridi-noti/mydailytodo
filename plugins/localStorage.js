import MemoryStorage from 'memorystorage';
import storageAvailable from 'storage-available';

class LocalStorageService {
  ls;

  constructor() {
    if (storageAvailable('localStorage')) {
      this.ls = localStorage;
    } else {
      this.ls = new MemoryStorage('my-daily-todo');
    }
  }

  setItem(key, value) {
    this.ls.setItem(key, value);
  }

  getItem(key) {
    return this.ls.getItem(key);
  }

  removeItem(key) {
    this.ls.removeItem(key);
  }
}

export default (ctx, inject) => {
  ctx.localStorage = new LocalStorageService();
  inject('localStorage', ctx.localStorage);
};
