import { dispatchResize } from '../store/resize';

export default ({ store }, _inject) => {
  dispatchResize(store);
  window.addEventListener('resize', () => {
    dispatchResize(store);
  });
};
