import { replaceItemWithItems } from '../utility/array';
import { findIndex } from 'lodash';
import { initTask, parseObjectToTask } from '../utility/task';
import { saveAsFile } from '../utility/file';

export const state = () => ({
  dailyTasks: []
});

export const getters = {
  dailyTasks: state => {
    return state.dailyTasks;
  }
};

const assignId = task => {
  if (!task.id) {
    task.id = new Date().getTime();
  }
};

export const mutations = {
  addTask(state, task) {
    const collection = state.dailyTasks || [];
    const newTask = task;
    assignId(newTask);
    state.dailyTasks = [...collection, newTask];
  },

  addTasks(state, tasks) {
    const collection = state.dailyTasks || [];
    const newTasks = tasks.map(task => {
      assignId(task);
      return task;
    });
    state.dailyTasks = [...collection, ...newTasks];
  },

  updateTask(state, task) {
    if (!task || !task.id) {
      return;
    }

    const matchId = item => item.id === task.id;
    state.dailyTasks = replaceItemWithItems(state.dailyTasks, matchId, [task]);
  },

  removeTask(state, task) {
    if (!task || !task.id) {
      return;
    }

    const matchId = item => item.id === task.id;
    state.dailyTasks = replaceItemWithItems(state.dailyTasks, matchId, []);
  },

  load(state, tasks) {
    state.dailyTasks = tasks || [];
  },

  reset(state, rootDefaultTasks) {
    const defaultTasks = rootDefaultTasks || [];
    const currentTasks = state.dailyTasks || [];
    const nonDefaultCarriedOverTasks = [];
    currentTasks.forEach(task => {
      if (!task.isCarriedOver) {
        return;
      }
      const defaultTaskIndex = findIndex(defaultTasks, defaultTask => defaultTask.id === task.id);
      if (defaultTaskIndex) {
        defaultTasks[defaultTaskIndex] = carryOver(task);
      } else {
        nonDefaultCarriedOverTasks.push(carryOver(task));
      }
    });
    state.dailyTasks = [...defaultTasks, ...nonDefaultCarriedOverTasks];
  }
};

const carryOver = task => ({
  ...task,
  isCarriedOver: false,
  isDone: false
});

const LOCAL_STORAGE_KEY = 'dailyTasks';

const saveDailyTasksToLS = (app, getters) => {
  app.$localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(getters.dailyTasks));
};

const getDailyTasksFromLS = app => {
  const taskString = app.$localStorage.getItem(LOCAL_STORAGE_KEY);
  let tasks = [];
  try {
    tasks = JSON.parse(taskString);
  } catch (error) {
    console.warn('Encountered a problem with parsing the daily task object');
    console.warn(error);
    app.$localStorage.removeItem(LOCAL_STORAGE_KEY);
  }
  tasks = tasks || [];
  return tasks;
};

const dispatchSave = dispatch => dispatch('save');

export const actions = {
  addTaskByName({ commit, dispatch }, name) {
    const task = initTask({ name });
    commit('addTask', task);
    dispatchSave(dispatch);
  },

  addTask({ commit, dispatch }, task) {
    commit('addTask', task);
    dispatchSave(dispatch);
  },

  addTasks({ commit, dispatch }, tasks) {
    commit('addTasks', tasks);
    dispatchSave(dispatch);
  },

  updateTask({ commit, dispatch }, task) {
    commit('updateTask', task);
    dispatchSave(dispatch);
  },

  removeTask({ commit, dispatch }, task) {
    commit('removeTask', task);
    dispatchSave(dispatch);
  },

  load({ commit, dispatch }) {
    const tasks = getDailyTasksFromLS(this);
    if (tasks.length) {
      commit('load', tasks);
      dispatchSave(dispatch);
    } else {
      dispatch('reset');
    }
  },

  replaceAllTasks({ commit, dispatch }, tasks) {
    commit('load', tasks);
    dispatchSave(dispatch);
  },

  reset({ commit, dispatch, rootGetters }) {
    commit('reset', rootGetters['defaultTasks/defaultTasks']);
    dispatchSave(dispatch);
  },

  save({ getters }) {
    saveDailyTasksToLS(this, getters);
  },

  downloadAsJson({ getters }, fileName) {
    const taskJson = JSON.stringify(getters.dailyTasks);
    saveAsFile(taskJson, fileName);
  },

  importFromJson({ commit }, jsonString) {
    const unparsedTasks = JSON.parse(jsonString);
    commit('load', unparsedTasks.map(task => parseObjectToTask(task)));
  }
};
