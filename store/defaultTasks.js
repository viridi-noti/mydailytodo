import { DEFAULT_TASKS } from '../constants/defaultTasks';
import { replaceItemWithItems } from '../utility/array';
import { initTask, parseObjectToTask } from '../utility/task';
import { saveAsFile } from '../utility/file';

export const state = () => ({
  defaultTasks: []
});

export const getters = {
  defaultTasks: state => {
    return state.defaultTasks;
  }
};

const assignId = task => {
  if (!task.id) {
    task.id = new Date().getTime();
  }
  return task;
};

export const mutations = {
  addTask(state, task) {
    const collection = state.defaultTasks || [];
    const newTask = task;
    assignId(newTask);
    state.defaultTasks = [...collection, newTask];
    localStorage.set;
  },

  addTasks(state, tasks) {
    const collection = state.defaultTasks || [];
    const newTasks = tasks.map(task => {
      return assignId(task);;
    });
    state.defaultTasks = [...collection, ...newTasks];
  },

  updateTask(state, task) {
    if (!task || !task.id) {
      return;
    }

    const matchId = item => item.id === task.id;
    state.defaultTasks = replaceItemWithItems(state.defaultTasks, matchId, [task]);
  },

  removeTask(state, task) {
    if (!task || !task.id) {
      return;
    }

    const matchId = item => item.id === task.id;
    state.defaultTasks = replaceItemWithItems(state.defaultTasks, matchId, []);
  },

  load(state, tasks) {
    state.defaultTasks = tasks || [];
  },

  hardReset(state) {
    state.defaultTasks = DEFAULT_TASKS.map(task => assignId(initTask(task)));
  }
};

const LOCAL_STORAGE_KEY = 'defaultTasks';

const saveDefaultTasksToLS = (app, getters) => {
  app.$localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(getters.defaultTasks));
};

const getDefaultTasksFromLS = app => {
  const taskString = app.$localStorage.getItem(LOCAL_STORAGE_KEY);
  let tasks = [];
  try {
    tasks = JSON.parse(taskString);
  } catch (error) {
    console.warn('Encountered a problem with parsing the default task object');
    console.warn(error);
    app.$localStorage.removeItem(LOCAL_STORAGE_KEY);
  }
  tasks = tasks || [];
  return tasks;
};

const dispatchSave = dispatch => dispatch('save');

export const actions = {
  addTaskByName({ commit, dispatch }, name) {
    const task = initTask({ name });
    commit('addTask', task);
    dispatchSave(dispatch);
  },

  addTask({ commit, dispatch }, task) {
    commit('addTask', task);
    dispatchSave(dispatch);
  },

  addTasks({ commit, dispatch }, tasks) {
    commit('addTasks', tasks);
    dispatchSave(dispatch);
  },

  updateTask({ commit, dispatch }, task) {
    commit('updateTask', task);
    dispatchSave(dispatch);
  },

  removeTask({ commit, dispatch }, task) {
    commit('removeTask', task);
    dispatchSave(dispatch);
  },

  load({ commit, dispatch }) {
    const tasks = getDefaultTasksFromLS(this);
    if (tasks.length) {
      commit('load', tasks);
      dispatchSave(dispatch);
    } else {
      dispatch('hardReset');
    }
  },

  replaceAllTasks({ commit, dispatch }, tasks) {
    commit('load', tasks);
    dispatchSave(dispatch);
  },

  hardReset({ commit, dispatch }) {
    commit('hardReset');
    dispatchSave(dispatch);
  },

  save({ getters }) {
    saveDefaultTasksToLS(this, getters);
  },

  downloadAsJson({ getters }, fileName) {
    const taskJson = JSON.stringify(getters.defaultTasks);
    saveAsFile(taskJson, fileName);
  },

  importFromJson({ commit }, jsonString) {
    const unparsedTasks = JSON.parse(jsonString);
    commit('load', unparsedTasks.map(task => parseObjectToTask(task)));
  }
};
