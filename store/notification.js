import { removeFirst } from '../utility/array';

export const state = () => ({
  messages: [],
  messageCloseTimeouts: []
});

export const getters = {
  messageCount: state => {
    if (state && state.messages && state.messages.length) {
      return state.messages.length;
    }
    return 0;
  },
  activeMessage: state => {
    if (state && state.messages && state.messages.length) {
      return state.messages[state.messages.length - 1];
    }
    return '';
  }
};

export const mutations = {
  addMessage(state, message) {
    if (!state.messages) {
      state.messages = [];
    }
    state.messages.push(message);
  },
  removeFirstMessage(state) {
    state.messages = removeFirst(state.messages);
  },

  addTimeout(state, timeoutIndex) {
    if (!state.messageCloseTimeouts) {
      state.messageCloseTimeouts = [];
    }
    state.messageCloseTimeouts.push(timeoutIndex)
  },
  removeFirstTimeout(state) {
    const timeouts = state.messageCloseTimeouts;
    if (timeouts && timeouts.length) {
      window.clearTimeout(timeouts[0])
    }
    state.messageCloseTimeouts = removeFirst(timeouts);
  },
};

const DEFAULT_AUTOCLOSE_WAIT = 3000;

export const actions = {
  success({ commit, dispatch }, text) {
    commit('addMessage', { message: text, type: 'success'});
    dispatch('addCloseTimeout');
  },
  closeEarly({ commit }) {
    commit('closeEarly');
  },
  addCloseTimeout({ commit, dispatch }, customTimeout) {
    const timeoutDuration = customTimeout !== null && customTimeout !== undefined ? customTimeout : DEFAULT_AUTOCLOSE_WAIT;
    const timeout = setTimeout(() => {
      dispatch('closeEarly');
    }, timeoutDuration);
    commit('addTimeout', timeout);
  },
  closeEarly({ commit }) {
    commit('removeFirstMessage');
    commit('removeFirstTimeout');
  }
}
