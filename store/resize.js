export const state = () => ({
  windowWidth: 0
});

export const mutations = {
  update(state, newWidth) {
    if (state.windowWidth !== newWidth) {
      state.windowWidth = newWidth;
    }
  }
};

export const actions = {
  update({ commit }, newWidth) {
    commit('update', newWidth);
  }
};

export const dispatchResize = store => {
  if (!store || !store.state || !store.state.resize) {
    return;
  }
  const windowWidth = store.state.resize.windowWidth;
  const newWidth = window.innerWidth;
  if (windowWidth !== newWidth) {
    store.dispatch('resize/update', newWidth);
  }
};
