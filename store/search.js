export const state = () => ({
  searchKeyword: ''
});

export const getters = {
  searchKeyword: state => {
    return state.searchKeyword;
  },
  isSearching: state => {
    return !!state.searchKeyword;
  }
};

export const mutations = {
  updateSearch(state, keyword) {
    state.searchKeyword = keyword;
  }
};

export const actions = {
  updateSearch({ commit }, keyword) {
    commit('updateSearch', keyword);
  },
  clearSearch({ commit }) {
    commit('updateSearch', '');
  }
}
