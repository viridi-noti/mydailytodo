const AVAILABLE_STATES = {
  NORMAL: 0,
  EDITING: 1,
  REORDER: 2
}

export const state = () => ({
  generalState: AVAILABLE_STATES.NORMAL
});

export const getters = {
  isEditing: state => {
    return state.generalState === AVAILABLE_STATES.EDITING;
  },
  isReordering: state => {
    return state.generalState === AVAILABLE_STATES.REORDER;
  },
};

export const mutations = {
  toEdit(state) {
    state.generalState = AVAILABLE_STATES.EDITING;
  },
  toNormal(state) {
    state.generalState = AVAILABLE_STATES.NORMAL;
  },
  toReorder(state) {
    state.generalState = AVAILABLE_STATES.REORDER;
  }
}

export const actions = {
  toggleEdit({ commit, getters }) {
    if (getters.isEditing) {
      commit('toNormal');
      return;
    }
    commit('toEdit');
  },
  toggleReorder({ commit, getters }) {
    if (getters.isReordering) {
      commit('toNormal');
      return;
    }
    commit('toReorder');
  },
  toNormal({ commit, getters }) {
    if (getters.isEditing || getters.isReordering) {
      commit('toNormal');
    }
  }
}
