import { findIndex } from 'lodash';

export const replaceItemWithItems = (collection, predicate, newItems) => {
  let newCollection = collection || [];
  const targetIndex = findIndex(newCollection, predicate);
  if (targetIndex === -1) {
    return newCollection;
  }
  newCollection = [...newCollection.slice(0, targetIndex), ...newItems, ...newCollection.slice(targetIndex + 1)];
  return newCollection;
};

export const removeFirst = array => {
  if (!array) {
    return [];
  }
  if (array.length) {
    return array.slice(1);
  }
};
