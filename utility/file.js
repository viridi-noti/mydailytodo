import { saveAs } from 'file-saver';

export const saveAsFile = (fileContent, fileName) => {
  const fileBlob = new Blob([fileContent], {type: "text/plain;charset=utf-8"});
  saveAs(fileBlob, fileName);
};

export const readAsText = (file) => {
  return new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = e => {
      resolve(e.target.result);
    };
    reader.readAsText(file);
  });
}
