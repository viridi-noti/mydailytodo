export const initTask = task => ({
  comment: '',
  deadline: '', // hour string hh:mm
  isPinned: false,
  isCarriedOver: false,
  isDone: false,
  ...task
});

export const hasRequiredProperty = task => task.id && task.name;

export const IMPORTABLE_PROPERTIES = ['comment', 'deadline', 'isCarriedOver', 'isDone', 'id', 'isPinned', 'name'];

export const parseObjectToTask = object => {
  let task = {};
  Object.keys(object).forEach(key => {
    if (IMPORTABLE_PROPERTIES.indexOf(key) !== -1) {
      task[key] = object[key];
    }
  });
  return task;
};
