export const timeStringToObject = (timeString) => {
  if (!timeString) {
    return null;
  }
  const [hour, minute] = timeString.split(':');
  if (isNaN(hour) || isNaN(minute)) {
    return null;
  }
  return {
    hour: Number(hour),
    minute: Number(minute)
  };
}

export const getCurrentTimeObject = () => {
  const now = new Date();
  return {
    hour: now.getHours(),
    minute: now.getMinutes()
  };
}

export const getTimeDifferenceToNowInMinutes = (timeString) => {
  const timeObject = timeStringToObject(timeString);
  if (!timeObject) {
    return null;
  }
  const { hour, minute } = timeObject;
  const { hour: currentHour, minute: currentMinute } = getCurrentTimeObject();
  return getTotalMinutes(hour, minute) - getTotalMinutes(currentHour, currentMinute);
}

export const getTimeDifferenceInText = (timeString) => {
  const differenceInMinutes = getTimeDifferenceToNowInMinutes(timeString);
  if (!differenceInMinutes) {
    return '';
  }
  const absoluteHours = Math.abs(differenceInMinutes);
  const MINUTES_IN_ONE_HOUR = 60;
  const differenceInHours = Math.floor(absoluteHours / MINUTES_IN_ONE_HOUR);
  const remainingMinutes = absoluteHours % MINUTES_IN_ONE_HOUR;
  let text = ''
  if (differenceInHours > 0) {
    text += `${differenceInHours} ${getUnitText(differenceInHours, 'hr', 'hrs')} `;
  }
  text += `${remainingMinutes} ${getUnitText(remainingMinutes, 'min', 'mins')} `;
  text += `${differenceInMinutes > 0 ? 'left' : 'past'}`
  return text;
}

const getUnitText = (value, singularUnit, pluralUnit) => {
  return value > 1 ? pluralUnit : singularUnit;
}

export const getTotalMinutes = (hour, minute) => {
  return hour * 60 + minute;
}
